volatile long TRACEBUFFER[256];
unsigned int bufIndex;
volatile long traceOut;

void pushDbg(int data,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | ((long )data);
}

void pushDbgCF(int ID)
{
  TRACEBUFFER[bufIndex++] = ID;
}

void pushDbgLong(long dataL,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | dataL >> 32;
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | (dataL & 0xffffffffUL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*************************************************************************/
/*                                                                       */
/*   SNU-RT Benchmark Suite for Worst Case Timing Analysis               */
/*   =====================================================               */
/*                              Collected and Modified by S.-S. Lim      */
/*                                           sslim@archi.snu.ac.kr       */
/*                                         Real-Time Research Group      */
/*                                        Seoul National University      */
/*                                                                       */
/*                                                                       */
/*        < Features > - restrictions for our experimental environment   */
/*                                                                       */
/*          1. Completely structured.                                    */
/*               - There are no unconditional jumps.                     */
/*               - There are no exit from loop bodies.                   */
/*                 (There are no 'break' or 'return' in loop bodies)     */
/*          2. No 'switch' statements.                                   */
/*          3. No 'do..while' statements.                                */
/*          4. Expressions are restricted.                               */
/*               - There are no multiple expressions joined by 'or',     */
/*                'and' operations.                                      */
/*          5. No library calls.                                         */
/*               - All the functions needed are implemented in the       */
/*                 source file.                                          */
/*                                                                       */
/*                                                                       */
/*************************************************************************/
/*                                                                       */
/*  FILE: adpcm.c                                                        */
/*  SOURCE : C Algorithms for Real-Time DSP by P. M. Embree              */
/*                                                                       */
/*  DESCRIPTION :                                                        */
/*                                                                       */
/*     CCITT G.722 ADPCM (Adaptive Differential Pulse Code Modulation)   */
/*     algorithm.                                                        */
/*     16khz sample rate data is stored in the array test_data[SIZE].    */
/*     Results are stored in the array compressed[SIZE] and result[SIZE].*/
/*     Execution time is determined by the constant SIZE (default value  */
/*     is 2000).                                                         */
/*                                                                       */
/*  REMARK :                                                             */
/*                                                                       */
/*  EXECUTION TIME :                                                     */
/*                                                                       */
/*                                                                       */
/*************************************************************************/
#include <stdio.h>
int encode(int xin1,int xin2);
void decode(int input);
int filtez(int *bpl,int *dlt);
void upzero(int dlt,int *dlti,int *bli);
int filtep(int rlt1,int al1,int rlt2,int al2);
int quantl(int el,int detl);
int logscl(int il,int nbl);
int scalel(int nbl,int shift_constant);
int uppol2(int al1,int al2,int plt,int plt1,int plt2);
int uppol1(int al1,int apl2,int plt,int plt1);
int logsch(int ih,int nbh);
void reset();
/* G722 C code */
/* variables for transimit quadrature mirror filter here */
int tqmf[24];
/* QMF filter coefficients:
scaled by a factor of 4 compared to G722 CCITT recomendation */
const int h[24] = {(12), (- 44), (- 44), (212), (48), (- 624), (128), (1448), (- 840), (- 3220), (3804), (15504), (15504), (3804), (- 3220), (- 840), (1448), (128), (- 624), (48), (212), (- 44), (- 44), (12)};
int xl;
int xh;
/* variables for receive quadrature mirror filter here */
int accumc[11];
int accumd[11];
/* outputs of decode() */
int xout1;
int xout2;
int xs;
int xd;
/* variables for encoder (hi and lo) here */
int il;
int szl;
int spl;
int sl;
int el;
const int qq4_code4_table[16] = {(0), (- 20456), (- 12896), (- 8968), (- 6288), (- 4240), (- 2584), (- 1200), (20456), (12896), (8968), (6288), (4240), (2584), (1200), (0)};
const int qq6_code6_table[64] = {(- 136), (- 136), (- 136), (- 136), (- 24808), (- 21904), (- 19008), (- 16704), (- 14984), (- 13512), (- 12280), (- 11192), (- 10232), (- 9360), (- 8576), (- 7856), (- 7192), (- 6576), (- 6000), (- 5456), (- 4944), (- 4464), (- 4008), (- 3576), (- 3168), (- 2776), (- 2400), (- 2032), (- 1688), (- 1360), (- 1040), (- 728), (24808), (21904), (19008), (16704), (14984), (13512), (12280), (11192), (10232), (9360), (8576), (7856), (7192), (6576), (6000), (5456), (4944), (4464), (4008), (3576), (3168), (2776), (2400), (2032), (1688), (1360), (1040), (728), (432), (136), (- 432), (- 136)};
int delay_bpl[6];
int delay_dltx[6];
const int wl_code_table[16] = {(- 60), (3042), (1198), (538), (334), (172), (58), (- 30), (3042), (1198), (538), (334), (172), (58), (- 30), (- 60)};
const int ilb_table[32] = {(2048), (2093), (2139), (2186), (2233), (2282), (2332), (2383), (2435), (2489), (2543), (2599), (2656), (2714), (2774), (2834), (2896), (2960), (3025), (3091), (3158), (3228), (3298), (3371), (3444), (3520), (3597), (3676), (3756), (3838), (3922), (4008)};
/* delay line */
int nbl;
int al1;
int al2;
int plt;
int plt1;
int plt2;
int dlt;
int rlt;
int rlt1;
int rlt2;
/* decision levels - pre-multiplied by 8, 0 to indicate end */
const int decis_levl[30] = {(280), (576), (880), (1200), (1520), (1864), (2208), (2584), (2960), (3376), (3784), (4240), (4696), (5200), (5712), (6288), (6864), (7520), (8184), (8968), (9752), (10712), (11664), (12896), (14120), (15840), (17560), (20456), (23352), (32767)};
int detl;
/* quantization table 31 long to make quantl look-up easier,
last entry is for mil=30 case when wd is max */
const int quant26bt_pos[31] = {(61), (60), (59), (58), (57), (56), (55), (54), (53), (52), (51), (50), (49), (48), (47), (46), (45), (44), (43), (42), (41), (40), (39), (38), (37), (36), (35), (34), (33), (32), (32)};
/* quantization table 31 long to make quantl look-up easier,
last entry is for mil=30 case when wd is max */
const int quant26bt_neg[31] = {(63), (62), (31), (30), (29), (28), (27), (26), (25), (24), (23), (22), (21), (20), (19), (18), (17), (16), (15), (14), (13), (12), (11), (10), (9), (8), (7), (6), (5), (4), (4)};
int deth;
/* this comes from adaptive predictor */
int sh;
int eh;
const int qq2_code2_table[4] = {(- 7408), (- 1616), (7408), (1616)};
const int wh_code_table[4] = {(798), (- 214), (798), (- 214)};
int dh;
int ih;
int nbh;
int szh;
int sph;
int ph;
int yh;
int rh;
int delay_dhx[6];
int delay_bph[6];
int ah1;
int ah2;
int ph1;
int ph2;
int rh1;
int rh2;
/* variables for decoder here */
int ilr;
int rl;
int dec_deth;
int dec_detl;
int dec_dlt;
int dec_del_bpl[6];
int dec_del_dltx[6];
int dec_plt;
int dec_plt1;
int dec_plt2;
int dec_szl;
int dec_spl;
int dec_sl;
int dec_rlt1;
int dec_rlt2;
int dec_rlt;
int dec_al1;
int dec_al2;
int dl;
int dec_nbl;
int dec_dh;
int dec_nbh;
/* variables used in filtez */
int dec_del_bph[6];
int dec_del_dhx[6];
int dec_szh;
/* variables used in filtep */
int dec_rh1;
int dec_rh2;
int dec_ah1;
int dec_ah2;
int dec_ph;
int dec_sph;
int dec_sh;
int dec_ph1;
int dec_ph2;
/* G722 encode function two ints in, one 8 bit output */
/* put input samples in xin1 = first value, xin2 = second value */
/* returns il and ih stored together */

int abs(int n)
{
  int m;
  if (n >= 0) {
    m = n;
    pushDbg(m,1);
  }
   else {
    m = -n;
    pushDbg(m,2);
  }
  return m;
}

int encode(int xin1,int xin2)
{
  int i;
  const int *h_ptr;
  int *tqmf_ptr;
  int *tqmf_ptr1;
  long xa;
  long xb;
  int decis;
/* transmit quadrature mirror filters implemented here */
  h_ptr = h;
  tqmf_ptr = tqmf;
  xa = ((long )( *(tqmf_ptr++))) * ( *(h_ptr++));
  pushDbgLong(xa,5);
  xb = ((long )( *(tqmf_ptr++))) * ( *(h_ptr++));
  pushDbgLong(xb,7);
/* main multiply accumulate loop for samples and coefficients */
  for (i = 0; i < 10; i++) {
    xa += ((long )( *(tqmf_ptr++))) * ( *(h_ptr++));
    pushDbgLong(xa,11);
    xb += ((long )( *(tqmf_ptr++))) * ( *(h_ptr++));
    pushDbgLong(xb,13);
  }
/* final mult/accumulate */
  xa += ((long )( *(tqmf_ptr++))) * ( *(h_ptr++));
  pushDbgLong(xa,15);
  xb += ((long )( *tqmf_ptr)) * ( *(h_ptr++));
  pushDbgLong(xb,17);
/* update delay line tqmf */
  tqmf_ptr1 = tqmf_ptr - 2;
  for (i = 0; i < 22; i++) 
     *(tqmf_ptr--) =  *(tqmf_ptr1--);
   *(tqmf_ptr--) = xin1;
   *tqmf_ptr = xin2;
/* scale outputs */
  xl = (xa + xb >> 15);
  pushDbg(xl,26);
  xh = (xa - xb >> 15);
  pushDbg(xh,29);
/* end of quadrature mirror filter code */
/* starting with lower sub band encoder */
/* filtez - compute predictor output section - zero section */
  szl = filtez(delay_bpl,delay_dltx);
  pushDbg(szl,32);
/* filtep - compute predictor output signal (pole section) */
  spl = filtep(rlt1,al1,rlt2,al2);
  pushDbg(spl,33);
/* compute the predictor output value in the lower sub_band encoder */
  sl = szl + spl;
  pushDbg(sl,34);
  el = xl - sl;
  pushDbg(el,36);
/* quantl: quantize the difference signal */
  il = quantl(el,detl);
  pushDbg(il,38);
/* computes quantized difference signal */
/* for invqbl, truncate by 2 lsbs, so mode = 3 */
  dlt = (((long )detl) * qq4_code4_table[il >> 2] >> 15);
  pushDbg(dlt,39);
/* logscl: updates logarithmic quant. scale factor in low sub band */
  nbl = logscl(il,nbl);
  pushDbg(nbl,44);
/* scalel: compute the quantizer scale factor in the lower sub band */
/* calling parameters nbl and 8 (constant such that scalel can be scaleh) */
  detl = scalel(nbl,8);
  pushDbg(detl,45);
/* parrec - simple addition to compute recontructed signal for adaptive pred */
  plt = dlt + szl;
  pushDbg(plt,46);
/* upzero: update zero section predictor coefficients (sixth order)*/
/* calling parameters: dlt, dlt1, dlt2, ..., dlt6 from dlt */
/*  bpli (linear_buffer in which all six values are delayed */
/* return params:      updated bpli, delayed dltx */
  upzero(dlt,delay_dltx,delay_bpl);
/* uppol2- update second predictor coefficient apl2 and delay it as al2 */
/* calling parameters: al1, al2, plt, plt1, plt2 */
  al2 = uppol2(al1,al2,plt,plt1,plt2);
  pushDbg(al2,48);
/* uppol1 :update first predictor coefficient apl1 and delay it as al1 */
/* calling parameters: al1, apl2, plt, plt1 */
  al1 = uppol1(al1,al2,plt,plt1);
  pushDbg(al1,49);
/* recons : compute recontructed signal for adaptive predictor */
  rlt = sl + dlt;
  pushDbg(rlt,50);
/* done with lower sub_band encoder; now implement delays for next time*/
  rlt2 = rlt1;
  pushDbg(rlt2,52);
  rlt1 = rlt;
  pushDbg(rlt1,53);
  plt2 = plt1;
  pushDbg(plt2,54);
  plt1 = plt;
  pushDbg(plt1,55);
/* high band encode */
  szh = filtez(delay_bph,delay_dhx);
  pushDbg(szh,56);
  sph = filtep(rh1,ah1,rh2,ah2);
  pushDbg(sph,57);
/* predic: sh = sph + szh */
  sh = sph + szh;
  pushDbg(sh,58);
/* subtra: eh = xh - sh */
  eh = xh - sh;
  pushDbg(eh,60);
/* quanth - quantization of difference signal for higher sub-band */
/* quanth: in-place for speed params: eh, deth (has init. value) */
  if (eh >= 0) {
/* 2,3 are pos codes */
    ih = 3;
  }
   else {
/* 0,1 are neg codes */
    ih = 1;
  }
  decis = (564L * ((long )deth) >> 12L);
  pushDbg(decis,65);
  if (abs(eh) > decis) {
/* mih = 2 case */
    ih--;
    pushDbg(ih,511);
  }
/* compute the quantized difference signal, higher sub-band*/
  dh = (((long )deth) * qq2_code2_table[ih] >> 15L);
  pushDbg(dh,69);
/* logsch: update logarithmic quantizer scale factor in hi sub-band*/
  nbh = logsch(ih,nbh);
  pushDbg(nbh,73);
/* note : scalel and scaleh use same code, different parameters */
  deth = scalel(nbh,10);
  pushDbg(deth,74);
/* parrec - add pole predictor output to quantized diff. signal */
  ph = dh + szh;
  pushDbg(ph,75);
/* upzero: update zero section predictor coefficients (sixth order) */
/* calling parameters: dh, dhi, bphi */
/* return params: updated bphi, delayed dhx */
  upzero(dh,delay_dhx,delay_bph);
/* uppol2: update second predictor coef aph2 and delay as ah2 */
/* calling params: ah1, ah2, ph, ph1, ph2 */
  ah2 = uppol2(ah1,ah2,ph,ph1,ph2);
  pushDbg(ah2,77);
/* uppol1:  update first predictor coef. aph2 and delay it as ah1 */
  ah1 = uppol1(ah1,ah2,ph,ph1);
  pushDbg(ah1,78);
/* recons for higher sub-band */
  yh = sh + dh;
  pushDbg(yh,79);
/* done with higher sub-band encoder, now Delay for next time */
  rh2 = rh1;
  pushDbg(rh2,81);
  rh1 = yh;
  pushDbg(rh1,82);
  ph2 = ph1;
  pushDbg(ph2,83);
  ph1 = ph;
  pushDbg(ph1,84);
/* multiplex ih and il to get signals together */
  return il | ih << 6;
}
/* decode function, result in xout1 and xout2 */

void decode(int input)
{
  int i;
/* qmf accumulators */
  long xa1;
  long xa2;
  const int *h_ptr;
  int *ac_ptr;
  int *ac_ptr1;
  int *ad_ptr;
  int *ad_ptr1;
/* split transmitted word from input into ilr and ih */
  ilr = input & 0x3f;
  pushDbg(ilr,87);
  ih = input >> 6;
  pushDbg(ih,89);
/* LOWER SUB_BAND DECODER */
/* filtez: compute predictor output for zero section */
  dec_szl = filtez(dec_del_bpl,dec_del_dltx);
  pushDbg(dec_szl,91);
/* filtep: compute predictor output signal for pole section */
  dec_spl = filtep(dec_rlt1,dec_al1,dec_rlt2,dec_al2);
  pushDbg(dec_spl,92);
  dec_sl = dec_spl + dec_szl;
  pushDbg(dec_sl,93);
/* compute quantized difference signal for adaptive predic */
  dec_dlt = (((long )dec_detl) * qq4_code4_table[ilr >> 2] >> 15);
  pushDbg(dec_dlt,95);
/* compute quantized difference signal for decoder output */
  dl = (((long )dec_detl) * qq6_code6_table[il] >> 15);
  pushDbg(dl,100);
  rl = dl + dec_sl;
  pushDbg(rl,104);
/* logscl: quantizer scale factor adaptation in the lower sub-band */
  dec_nbl = logscl(ilr,dec_nbl);
  pushDbg(dec_nbl,106);
/* scalel: computes quantizer scale factor in the lower sub band */
  dec_detl = scalel(dec_nbl,8);
  pushDbg(dec_detl,107);
/* parrec - add pole predictor output to quantized diff. signal */
/* for partially reconstructed signal */
  dec_plt = dec_dlt + dec_szl;
  pushDbg(dec_plt,108);
/* upzero: update zero section predictor coefficients */
  upzero(dec_dlt,dec_del_dltx,dec_del_bpl);
/* uppol2: update second predictor coefficient apl2 and delay it as al2 */
  dec_al2 = uppol2(dec_al1,dec_al2,dec_plt,dec_plt1,dec_plt2);
  pushDbg(dec_al2,110);
/* uppol1: update first predictor coef. (pole setion) */
  dec_al1 = uppol1(dec_al1,dec_al2,dec_plt,dec_plt1);
  pushDbg(dec_al1,111);
/* recons : compute recontructed signal for adaptive predictor */
  dec_rlt = dec_sl + dec_dlt;
  pushDbg(dec_rlt,112);
/* done with lower sub band decoder, implement delays for next time */
  dec_rlt2 = dec_rlt1;
  pushDbg(dec_rlt2,114);
  dec_rlt1 = dec_rlt;
  pushDbg(dec_rlt1,115);
  dec_plt2 = dec_plt1;
  pushDbg(dec_plt2,116);
  dec_plt1 = dec_plt;
  pushDbg(dec_plt1,117);
/* HIGH SUB-BAND DECODER */
/* filtez: compute predictor output for zero section */
  dec_szh = filtez(dec_del_bph,dec_del_dhx);
  pushDbg(dec_szh,118);
/* filtep: compute predictor output signal for pole section */
  dec_sph = filtep(dec_rh1,dec_ah1,dec_rh2,dec_ah2);
  pushDbg(dec_sph,119);
/* predic:compute the predictor output value in the higher sub_band decoder */
  dec_sh = dec_sph + dec_szh;
  pushDbg(dec_sh,120);
/* in-place compute the quantized difference signal */
  dec_dh = (((long )dec_deth) * qq2_code2_table[ih] >> 15L);
  pushDbg(dec_dh,122);
/* logsch: update logarithmic quantizer scale factor in hi sub band */
  dec_nbh = logsch(ih,dec_nbh);
  pushDbg(dec_nbh,126);
/* scalel: compute the quantizer scale factor in the higher sub band */
  dec_deth = scalel(dec_nbh,10);
  pushDbg(dec_deth,127);
/* parrec: compute partially recontructed signal */
  dec_ph = dec_dh + dec_szh;
  pushDbg(dec_ph,128);
/* upzero: update zero section predictor coefficients */
  upzero(dec_dh,dec_del_dhx,dec_del_bph);
/* uppol2: update second predictor coefficient aph2 and delay it as ah2 */
  dec_ah2 = uppol2(dec_ah1,dec_ah2,dec_ph,dec_ph1,dec_ph2);
  pushDbg(dec_ah2,130);
/* uppol1: update first predictor coef. (pole setion) */
  dec_ah1 = uppol1(dec_ah1,dec_ah2,dec_ph,dec_ph1);
  pushDbg(dec_ah1,131);
/* recons : compute recontructed signal for adaptive predictor */
  rh = dec_sh + dec_dh;
  pushDbg(rh,132);
/* done with high band decode, implementing delays for next time here */
  dec_rh2 = dec_rh1;
  pushDbg(dec_rh2,134);
  dec_rh1 = rh;
  pushDbg(dec_rh1,135);
  dec_ph2 = dec_ph1;
  pushDbg(dec_ph2,136);
  dec_ph1 = dec_ph;
  pushDbg(dec_ph1,137);
/* end of higher sub_band decoder */
/* end with receive quadrature mirror filters */
  xd = rl - rh;
  pushDbg(xd,138);
  xs = rl + rh;
  pushDbg(xs,140);
/* receive quadrature mirror filters implemented here */
  h_ptr = h;
  ac_ptr = accumc;
  ad_ptr = accumd;
  xa1 = ((long )xd) * ( *(h_ptr++));
  pushDbgLong(xa1,145);
  xa2 = ((long )xs) * ( *(h_ptr++));
  pushDbgLong(xa2,147);
/* main multiply accumulate loop for samples and coefficients */
  for (i = 0; i < 10; i++) {
    xa1 += ((long )( *(ac_ptr++))) * ( *(h_ptr++));
    pushDbgLong(xa1,151);
    xa2 += ((long )( *(ad_ptr++))) * ( *(h_ptr++));
    pushDbgLong(xa2,153);
  }
/* final mult/accumulate */
  xa1 += ((long )( *ac_ptr)) * ( *(h_ptr++));
  pushDbgLong(xa1,155);
  xa2 += ((long )( *ad_ptr)) * ( *(h_ptr++));
  pushDbgLong(xa2,157);
/* scale by 2^14 */
  xout1 = (xa1 >> 14);
  pushDbg(xout1,159);
  xout2 = (xa2 >> 14);
  pushDbg(xout2,161);
/* update delay lines */
  ac_ptr1 = ac_ptr - 1;
  ad_ptr1 = ad_ptr - 1;
  for (i = 0; i < 10; i++) {
     *(ac_ptr--) =  *(ac_ptr1--);
     *(ad_ptr--) =  *(ad_ptr1--);
  }
   *ac_ptr = xd;
   *ad_ptr = xs;
}
/* clear all storage locations */

void reset()
{
  int i;
/* reset to min scale factor */
  detl = dec_detl = 32;
  deth = dec_deth = 8;
  nbl = al1 = al2 = plt1 = plt2 = rlt1 = rlt2 = 0;
  nbh = ah1 = ah2 = ph1 = ph2 = rh1 = rh2 = 0;
  dec_nbl = dec_al1 = dec_al2 = dec_plt1 = dec_plt2 = dec_rlt1 = dec_rlt2 = 0;
  dec_nbh = dec_ah1 = dec_ah2 = dec_ph1 = dec_ph2 = dec_rh1 = dec_rh2 = 0;
  for (i = 0; i < 6; i++) {
    delay_dltx[i] = 0;
    delay_dhx[i] = 0;
    dec_del_dltx[i] = 0;
    dec_del_dhx[i] = 0;
  }
  for (i = 0; i < 6; i++) {
    delay_bpl[i] = 0;
    delay_bph[i] = 0;
    dec_del_bpl[i] = 0;
    dec_del_bph[i] = 0;
  }
  for (i = 0; i < 24; i++) {
// i<23
    tqmf[i] = 0;
  }
  for (i = 0; i < 11; i++) {
    accumc[i] = 0;
    accumd[i] = 0;
  }
}
/* filtez - compute predictor output signal (zero section) */
/* input: bpl1-6 and dlt1-6, output: szl */

int filtez(int *bpl,int *dlt)
{
  int i;
  long zl;
  zl = ((long )( *(bpl++))) * ( *(dlt++));
  pushDbgLong(zl,235);
  for (i = 1; i < 6; i++) {
    zl += ((long )( *(bpl++))) * ( *(dlt++));
    pushDbgLong(zl,239);
  }
/* x2 here */
  return (int )(zl >> 14);
}
/* filtep - compute predictor output signal (pole section) */
/* input rlt1-2 and al1-2, output spl */

int filtep(int rlt1,int al1,int rlt2,int al2)
{
  long pl;
  long pl2;
  pl = (2 * rlt1);
  pushDbgLong(pl,242);
  pl = ((long )al1) * pl;
  pushDbgLong(pl,244);
  pl2 = (2 * rlt2);
  pushDbgLong(pl2,246);
  pl += ((long )al2) * pl2;
  pushDbgLong(pl,248);
  return (int )(pl >> 15);
}
/* quantl - quantize the difference signal in the lower sub-band */

int quantl(int el,int detl)
{
  int ril;
  int mil;
  long wd;
  long decis;
/* abs of difference signal */
  wd = (abs(el));
  pushDbgLong(wd,251);
/* determine mil based on decision levels and detl gain */
  for (mil = 0; mil < 30; mil++) {
    decis = decis_levl[mil] * ((long )detl) >> 15L;
    pushDbgLong(decis,254);
    if (wd <= decis) 
      break; 
  }
/* if mil=30 then wd is less than all decision levels */
  if (el >= 0) {
    ril = quant26bt_pos[mil];
    pushDbg(ril,260);
  }
   else {
    ril = quant26bt_neg[mil];
    pushDbg(ril,262);
  }
  return ril;
}
/* logscl - update log quantizer scale factor in lower sub-band */
/* note that nbl is passed and returned */

int logscl(int il,int nbl)
{
  long wd;
/* leak factor 127/128 */
  wd = ((long )nbl) * 127L >> 7L;
  pushDbgLong(wd,264);
  nbl = ((int )wd) + wl_code_table[il >> 2];
  pushDbg(nbl,267);
  if (nbl < 0) 
    nbl = 0;
  if (nbl > 18432) 
    nbl = 18432;
  return nbl;
}
/* scalel: compute quantizer scale factor in lower or upper sub-band*/

int scalel(int nbl,int shift_constant)
{
  int wd1;
  int wd2;
  int wd3;
  wd1 = nbl >> 6 & 31;
  pushDbg(wd1,275);
  wd2 = nbl >> 11;
  pushDbg(wd2,278);
  wd3 = ilb_table[wd1] >> shift_constant + 1 - wd2;
  pushDbg(wd3,280);
  return wd3 << 3;
}
/* upzero - inputs: dlt, dlti[0-5], bli[0-5], outputs: updated bli[0-5] */
/* also implements delay of bli and update of dlti from dlt */

void upzero(int dlt,int *dlti,int *bli)
{
  int i;
  int wd2;
  int wd3;
/*if dlt is zero, then no sum into bli */
  if (dlt == 0) {
    for (i = 0; i < 6; i++) {
/* leak factor of 255/256 */
      bli[i] = ((int )(255L * bli[i] >> 8L));
    }
  }
   else {
    for (i = 0; i < 6; i++) {
      if (((long )dlt) * dlti[i] >= 0) 
        wd2 = 128;
       else {
        wd2 = - 128;
        pushDbg(wd2,300);
      }
/* leak factor of 255/256 */
      wd3 = ((int )(255L * bli[i] >> 8L));
      pushDbg(wd3,301);
      bli[i] = wd2 + wd3;
    }
  }
/* implement delay line for dlt */
  dlti[5] = dlti[4];
  dlti[4] = dlti[3];
  dlti[3] = dlti[2];
  dlti[1] = dlti[0];
  dlti[0] = dlt;
}
/* uppol2 - update second predictor coefficient (pole section) */
/* inputs: al1, al2, plt, plt1, plt2. outputs: apl2 */

int uppol2(int al1,int al2,int plt,int plt1,int plt2)
{
  long wd2;
  long wd4;
  int apl2;
  wd2 = 4L * ((long )al1);
  pushDbgLong(wd2,322);
  if (((long )plt) * plt1 >= 0L) {
/* check same sign */
    wd2 = -wd2;
    pushDbgLong(wd2,326);
  }
/* gain of 1/128 */
  wd2 = wd2 >> 7;
  pushDbgLong(wd2,327);
  if (((long )plt) * plt2 >= 0L) {
/* same sign case */
    wd4 = wd2 + 128;
    pushDbgLong(wd4,331);
  }
   else {
    wd4 = wd2 - 128;
    pushDbgLong(wd4,333);
  }
/* leak factor of 127/128 */
  apl2 = (wd4 + (127L * ((long )al2) >> 7L));
  pushDbg(apl2,335);
/* apl2 is limited to +-.75 */
  if (apl2 > 12288) 
    apl2 = 12288;
  if (apl2 < - 12288) {
    apl2 = - 12288;
    pushDbg(apl2,342);
  }
  return apl2;
}
/* uppol1 - update first predictor coefficient (pole section) */
/* inputs: al1, apl2, plt, plt1. outputs: apl1 */

int uppol1(int al1,int apl2,int plt,int plt1)
{
  long wd2;
  int wd3;
  int apl1;
/* leak factor of 255/256 */
  wd2 = ((long )al1) * 255L >> 8L;
  pushDbgLong(wd2,343);
  if (((long )plt) * plt1 >= 0L) {
/* same sign case */
    apl1 = ((int )wd2) + 192;
    pushDbg(apl1,348);
  }
   else {
    apl1 = ((int )wd2) - 192;
    pushDbg(apl1,350);
  }
/* note: wd3= .9375-.75 is always positive */
/* limit value */
  wd3 = 15360 - apl2;
  pushDbg(wd3,352);
  if (apl1 > wd3) {
    apl1 = wd3;
    pushDbg(apl1,355);
  }
  if (apl1 < -wd3) {
    apl1 = -wd3;
    pushDbg(apl1,357);
  }
  return apl1;
}
/* logsch - update log quantizer scale factor in higher sub-band */
/* note that nbh is passed and returned */

int logsch(int ih,int nbh)
{
  int wd;
/* leak factor 127/128 */
  wd = (((long )nbh) * 127L >> 7L);
  pushDbg(wd,358);
  nbh = wd + wh_code_table[ih];
  pushDbg(nbh,361);
  if (nbh < 0) {
    nbh = 0;
  }
  if (nbh > 22528) {
    nbh = 22528;
  }
  return nbh;
}
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     test_data : input data                                               |
|     test_compressed : expected output data for "encode"                  |
|     test_result : expected output data for "decode"                      |
+--------------------------------------------------------------------------+
*/
#define SIZE 100
#define IN_END 100
const int test_data[100] = {(0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x44), (0x43), (0x43), (0x43), (0x43), (0x43), (0x43), (0x43), (0x42), (0x42), (0x42), (0x42), (0x42), (0x42), (0x41), (0x41), (0x41), (0x41), (0x41), (0x40), (0x40), (0x40), (0x40), (0x40), (0x40), (0x40), (0x40), (0x3f), (0x3f), (0x3f), (0x3f), (0x3f), (0x3e), (0x3e), (0x3e), (0x3e), (0x3e), (0x3e), (0x3d), (0x3d), (0x3d), (0x3d), (0x3d), (0x3d), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3b), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c), (0x3c)};
int compressed[100];
int result[100];
const int test_compressed[100] = {(253), (222), (119), (186), (244), (146), (32), (160), (236), (237), (238), (240), (241), (241), (242), (243), (244), (243), (244), (245), (244), (244), (245), (245), (245), (246), (246), (247), (247), (247), (247), (248), (246), (247), (249), (247), (248), (247), (248), (247), (248), (247), (248), (247), (248), (248), (246), (248), (247), (248), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)};
const int test_result[100] = {(0), (- 1), (- 1), (0), (0), (- 1), (0), (0), (- 1), (- 1), (0), (0), (1), (1), (0), (- 2), (- 1), (- 2), (0), (- 3), (1), (0), (0), (- 4), (1), (1), (2), (11), (20), (18), (20), (22), (28), (27), (31), (31), (34), (31), (34), (34), (38), (37), (42), (42), (44), (41), (43), (42), (47), (45), (47), (44), (45), (43), (46), (45), (48), (46), (49), (48), (51), (49), (52), (52), (55), (53), (56), (55), (58), (57), (59), (57), (60), (60), (60), (54), (54), (53), (60), (62), (62), (54), (55), (56), (59), (53), (54), (56), (59), (53), (56), (58), (59), (53), (56), (58), (60), (54), (55), (57)};

void adpcm_main()
{
  int i;
  int j;
/* reset, initialize required memory */
  reset();
  j = 10;
  for (i = 0; i < 100; i += 2) {
    compressed[i / 2] = encode(test_data[i],test_data[i + 1]);
  }
  for (i = 0; i < 100; i += 2) {
    decode(compressed[i / 2]);
    result[i] = xout1;
    result[i + 1] = xout2;
  }
}

int main()
{
  int i;
  int main_result;
  main_result = 0;
  adpcm_main();
  for (i = 0; i < 100 / 2; i++) {
    if (compressed[i] == test_compressed[i]) {
      main_result++;
      pushDbg(main_result,639);
    }
  }
  for (i = 0; i < 100; i++) {
    if (result[i] == test_result[i]) {
      main_result++;
      pushDbg(main_result,641);
    }
  }
  return main_result;
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
}
