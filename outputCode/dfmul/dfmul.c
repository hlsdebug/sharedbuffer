volatile long TRACEBUFFER[256];
unsigned int bufIndex;
volatile long traceOut;

void pushDbg(int data,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | ((long )data);
}

void pushDbgCF(int ID)
{
  TRACEBUFFER[bufIndex++] = ID;
}

void pushDbgLong(long dataL,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | dataL >> 32;
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | (dataL & 0xffffffffUL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
#include "softfloat.c"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     a_input, b_input : input data                                        |
|     z_output : expected output data                                      |
+--------------------------------------------------------------------------+
*/
#define N 20
const float64 a_input[20] = {(0x7FF0000000000000ULL), (0x7FFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x4000000000000000ULL), (0x3FD0000000000000ULL), (0xC000000000000000ULL), (0xBFD0000000000000ULL), (0x4000000000000000ULL), (0xBFD0000000000000ULL), (0xC000000000000000ULL), (0x3FD0000000000000ULL), (0x0000000000000000ULL)
/* inf */
/* nan */
/* inf */
/* inf */
/* 1.0 */
/* 0.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 1.0 */
/* 1.0 */
/* 2.0 */
/* 0.25 */
/* -2.0 */
/* -0.25 */
/* 2.0 */
/* -0.25 */
/* -2.0 */
/* 0.25 */
/* 0.0 */
};
const float64 b_input[20] = {(0xFFFFFFFFFFFFFFFFULL), (0xFFF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0xFFFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FD0000000000000ULL), (0x4000000000000000ULL), (0xBFD0000000000000ULL), (0xC000000000000000ULL), (0xBFD0000000000000ULL), (0x4000000000000000ULL), (0x3FD0000000000000ULL), (0xC000000000000000ULL), (0x0000000000000000ULL)
/* nan */
/* -inf */
/* nan */
/* -inf */
/* nan */
/* inf */
/* inf */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 0.25 */
/* 2.0 */
/* -0.25 */
/* -2.0 */
/* -0.25 */
/* -2.0 */
/* 0.25 */
/* -2.0 */
/* 0.0 */
};
const float64 z_output[20] = {(0xFFFFFFFFFFFFFFFFULL), (0x7FFF000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0xFFFF000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0x0000000000000000ULL)
/* nan */
/* nan */
/* nan */
/* inf */
/* nan */
/* nan */
/* inf */
/* 0.0 */
/* -0.0 */
/* 0.0 */
/* -0.0 */
/* 0.5 */
/* 0.5 */
/* 0.5 */
/* 0.5 */
/* -0.5 */
/* -0.5 */
/* -0.5 */
/* -0.5 */
/* 0.0 */
};

int main()
{
  int main_result;
  int i;
  float64 x1;
  float64 x2;
  main_result = 0;
  for (i = 0; i < 20; i++) {
    float64 result;
    x1 = a_input[i];
    x2 = b_input[i];
    result = float64_mul(x1,x2);
    main_result += result == z_output[i];
    pushDbg(main_result,167);
    printf("a_input=%016llx b_input=%016llx expected=%016llx output=%016llx\n",a_input[i],b_input[i],z_output[i],result);
  }
  printf("Result: %d\n",main_result);
  if (main_result == 20) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
}
