volatile long TRACEBUFFER[256];
unsigned int bufIndex;
volatile long traceOut;

void pushDbg(int data,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | ((long )data);
}

void pushDbgCF(int ID)
{
  TRACEBUFFER[bufIndex++] = ID;
}

void pushDbgLong(long dataL,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | dataL >> 32;
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | (dataL & 0xffffffffUL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
* Copyright (C) 2008
* Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
* Nagoya University, Japan
* All rights reserved.
*
* Disclaimer of Warranty
*
* These software programs are available to the user without any license fee or
* royalty on an "as is" basis. The authors disclaims any and all warranties, 
* whether express, implied, or statuary, including any implied warranties or 
* merchantability or of fitness for a particular purpose. In no event shall the
* copyright-holder be liable for any incidental, punitive, or consequential damages
* of any kind whatsoever arising from the use of these programs. This disclaimer
* of warranty extends to the user of these programs and user's customers, employees,
* agents, transferees, successors, and assigns.
*
*/
#include <stdio.h>
int main_result;
#define R 0
#define ADDU 33
#define SUBU 35
#define MULT 24
#define MULTU 25
#define MFHI 16
#define MFLO 18
#define AND 36
#define OR 37
#define XOR 38
#define SLL 0
#define SRL 2
#define SLLV 4
#define SRLV 6
#define SLT 42
#define SLTU 43
#define JR 8
#define J 2
#define JAL 3
#define ADDIU 9
#define ANDI 12
#define ORI 13
#define XORI 14
#define LW 35
#define SW 43
#define LUI 15
#define BEQ 4
#define BNE 5
#define BGEZ 1
#define SLTI 10
#define SLTIU 11
#include "imem.h"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     A : input data                                                       |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const int A[8] = {(22), (5), (- 9), (3), (- 17), (38), (0), (11)};
const int outData[8] = {(- 17), (- 9), (0), (3), (5), (11), (22), (38)};
#define IADDR(x)	(((x)&0x000000ff)>>2)
#define DADDR(x)	(((x)&0x000000ff)>>2)

int main()
{
  long long hilo;
  int reg[32];
  int Hi = 0;
  int Lo = 0;
  int pc = 0;
  int dmem[64] = {(0)};
  int j;
  unsigned int ins;
  int op;
  int rs;
  int rt;
  int rd;
  int shamt;
  int funct;
  short address;
  int tgtadr;
  while(1){
    int i;
    int n_inst;
    n_inst = 0;
    main_result = 0;
    for (i = 0; i < 32; i++) {
      reg[i] = 0;
    }
    reg[29] = 0x7fffeffc;
    for (i = 0; i < 8; i++) {
      dmem[i] = A[i];
    }
    pc = 0x00400000;
    do {
      ins = imem[(pc & 0x000000ff) >> 2];
      pushDbg(ins,14);
      pc = pc + 4;
      pushDbg(pc,18);
      op = (ins >> 26);
      pushDbg(op,20);
      switch(op){
        case 0:
{
          funct = (ins & 0x3f);
          pushDbg(funct,22);
          shamt = (ins >> 6 & 0x1f);
          pushDbg(shamt,24);
          rd = (ins >> 11 & 0x1f);
          pushDbg(rd,27);
          rt = (ins >> 16 & 0x1f);
          pushDbg(rt,30);
          rs = (ins >> 21 & 0x1f);
          pushDbg(rs,33);
          switch(funct){
            case 33:
{
              reg[rd] = reg[rs] + reg[rt];
              break; 
            }
            case 35:
{
              reg[rd] = reg[rs] - reg[rt];
              break; 
            }
            case 24:
{
              hilo = ((long long )reg[rs]) * ((long long )reg[rt]);
              Lo = (hilo & 0x00000000ffffffffULL);
              pushDbg(Lo,50);
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              pushDbg(Hi,52);
              break; 
            }
            case 25:
{
              hilo = (((unsigned long long )reg[rs]) * ((unsigned long long )reg[rt]));
              Lo = (hilo & 0x00000000ffffffffULL);
              pushDbg(Lo,59);
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              pushDbg(Hi,61);
              break; 
            }
            case 16:
{
              reg[rd] = Hi;
              break; 
            }
            case 18:
{
              reg[rd] = Lo;
              break; 
            }
            case 36:
{
              reg[rd] = reg[rs] & reg[rt];
              break; 
            }
            case 37:
{
              reg[rd] = reg[rs] | reg[rt];
              break; 
            }
            case 38:
{
              reg[rd] = reg[rs] ^ reg[rt];
              break; 
            }
            case 0:
{
              reg[rd] = reg[rt] << shamt;
              break; 
            }
            case 2:
{
              reg[rd] = reg[rt] >> shamt;
              break; 
            }
            case 4:
{
              reg[rd] = reg[rt] << reg[rs];
              break; 
            }
            case 6:
{
              reg[rd] = reg[rt] >> reg[rs];
              break; 
            }
            case 42:
{
              reg[rd] = reg[rs] < reg[rt];
              break; 
            }
            case 43:
{
              reg[rd] = ((unsigned int )reg[rs]) < ((unsigned int )reg[rt]);
              break; 
            }
            case 8:
{
              pc = reg[rs];
              pushDbg(pc,111);
              break; 
            }
            default:
{
// error
              pc = 0;
              break; 
            }
          }
          break; 
        }
        case 2:
{
          tgtadr = (ins & 0x3ffffff);
          pushDbg(tgtadr,114);
          pc = tgtadr << 2;
          pushDbg(pc,116);
          break; 
        }
        case 3:
{
          tgtadr = (ins & 0x3ffffff);
          pushDbg(tgtadr,118);
          reg[31] = pc;
          pc = tgtadr << 2;
          pushDbg(pc,122);
          break; 
        }
        default:
{
          address = (ins & 0xffff);
          rt = (ins >> 16 & 0x1f);
          pushDbg(rt,126);
          rs = (ins >> 21 & 0x1f);
          pushDbg(rs,129);
          switch(op){
            case 9:
{
              reg[rt] = reg[rs] + address;
              break; 
            }
            case 12:
{
              reg[rt] = reg[rs] & ((unsigned short )address);
              break; 
            }
            case 13:
{
              reg[rt] = reg[rs] | ((unsigned short )address);
              break; 
            }
            case 14:
{
              reg[rt] = reg[rs] ^ ((unsigned short )address);
              break; 
            }
            case 35:
{
              reg[rt] = dmem[(reg[rs] + address & 0x000000ff) >> 2];
              break; 
            }
            case 43:
{
              dmem[(reg[rs] + address & 0x000000ff) >> 2] = reg[rt];
              break; 
            }
            case 15:
{
              reg[rt] = address << 16;
              break; 
            }
            case 4:
{
              if (reg[rs] == reg[rt]) {
                pc = pc - 4 + (address << 2);
                pushDbg(pc,168);
              }
              break; 
            }
            case 5:
{
              if (reg[rs] != reg[rt]) {
                pc = pc - 4 + (address << 2);
                pushDbg(pc,175);
              }
              break; 
            }
            case 1:
{
              if (reg[rs] >= 0) {
                pc = pc - 4 + (address << 2);
                pushDbg(pc,181);
              }
              break; 
            }
            case 10:
{
              reg[rt] = reg[rs] < address;
              break; 
            }
            case 11:
{
              reg[rt] = ((unsigned int )reg[rs]) < ((unsigned short )address);
              break; 
            }
            default:
{
/* error */
              pc = 0;
              break; 
            }
          }
          break; 
        }
      }
      reg[0] = 0;
      n_inst = n_inst + 1;
      pushDbg(n_inst,196);
    }while (pc != 0);
    main_result += n_inst == 611;
    pushDbg(main_result,199);
    for (j = 0; j < 8; j++) {
      main_result += dmem[j] == outData[j];
      pushDbg(main_result,203);
    }
    printf("Result: %d\n",main_result);
    if (main_result == 9) {
      printf("RESULT: PASS\n");
    }
     else {
      printf("RESULT: FAIL\n");
    }
    return main_result;
  }
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
}
