volatile long TRACEBUFFER[256];
unsigned int bufIndex;
volatile long traceOut;

void pushDbg(int data,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | ((long )data);
}

void pushDbgCF(int ID)
{
  TRACEBUFFER[bufIndex++] = ID;
}

void pushDbgLong(long dataL,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | dataL >> 32;
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | (dataL & 0xffffffffUL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
const int inPMV[2][2][2] = {{{(45), (207)}, {(70), (41)}}, {{(4), (180)}, {(120), (216)}}};
const int inmvfs[2][2] = {{(232), (200)}, {(32), (240)}};
const int outPMV[2][2][2] = {{{(1566), (206)}, {(70), (41)}}, {{(1566), (206)}, {(120), (216)}}};
const int outmvfs[2][2] = {{(0), (200)}, {(0), (240)}};
int evalue;
#include "config.h"
#include "global.h"
#include "getbits.c"
#include "getvlc.h"
#include "getvlc.c"
#include "motion.c"

void Initialize_Buffer()
{
  ld_Incnt = 0;
  ld_Rdptr = ld_Rdbfr + 2048;
  ld_Rdmax = ld_Rdptr;
  ld_Bfr = 68157440;
  pushDbg(ld_Bfr,161);
/* fills valid data into bfr */
  Flush_Buffer(0);
}

int main()
{
  int i;
  int j;
  int k;
  int main_result;
  int PMV[2][2][2];
  int dmvector[2];
  int motion_vertical_field_select[2][2];
  int s;
  int motion_vector_count;
  int mv_format;
  int h_r_size;
  int v_r_size;
  int dmv;
  int mvscale;
  main_result = 0;
  evalue = 0;
  System_Stream_Flag = 0;
  s = 0;
  motion_vector_count = 1;
  mv_format = 0;
  h_r_size = 200;
  v_r_size = 200;
  dmv = 0;
  mvscale = 1;
  for (i = 0; i < 2; i++) {
    dmvector[i] = 0;
    for (j = 0; j < 2; j++) {
      motion_vertical_field_select[i][j] = inmvfs[i][j];
      for (k = 0; k < 2; k++) 
        PMV[i][j][k] = inPMV[i][j][k];
    }
  }
  Initialize_Buffer();
  motion_vectors(PMV,dmvector,motion_vertical_field_select,s,motion_vector_count,mv_format,h_r_size,v_r_size,dmv,mvscale);
  for (i = 0; i < 2; i++) 
    for (j = 0; j < 2; j++) {
      main_result += motion_vertical_field_select[i][j] == outmvfs[i][j];
      pushDbg(main_result,196);
      for (k = 0; k < 2; k++) {
        main_result += PMV[i][j][k] == outPMV[i][j][k];
        pushDbg(main_result,204);
      }
    }
  printf("Result: %d\n",main_result);
  if (main_result == 12) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
}
