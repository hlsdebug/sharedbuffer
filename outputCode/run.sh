#!/bin/bash

rm ./*/*c

cd adpcm
../../addShBuf ../../chstone/adpcm/adpcm.c &> log2.log
cd ../aes
../../addShBuf ../../chstone/aes/aes.c ../../chstone/aes/aes_dec.c ../../chstone/aes/aes_enc.c ../../chstone/aes/aes_func.c ../../chstone/aes/aes_key.c &> log2.log
cd ../blowfish
../../addShBuf ../../chstone/blowfish/bf.c ../../chstone/blowfish/bf_cfb64.c ../../chstone/blowfish/bf_enc.c ../../chstone/blowfish/bf_skey.c &> log2.log
cd ../dfadd
../../addShBuf ../../chstone/dfadd/dfadd.c ../../chstone/dfadd/softfloat.c &> log2.log
cd ../dfdiv
../../addShBuf ../../chstone/dfdiv/dfdiv.c ../../chstone/dfdiv/softfloat.c &> log2.log
cd ../dfmul
../../addShBuf ../../chstone/dfmul/dfmul.c ../../chstone/dfmul/softfloat.c &> log2.log
cd ../dfsin
../../addShBuf ../../chstone/dfsin/dfsin.c ../../chstone/dfsin/softfloat.c &> log2.log
cd ../gsm
../../addShBuf ../../chstone/gsm/gsm.c ../../chstone/gsm/add.c ../../chstone/gsm/lpc.c &> log2.log
cd ../jpeg
../../addShBuf ../../chstone/jpeg/main.c ../../chstone/jpeg/chenidct.c ../../chstone/jpeg/decode.c ../../chstone/jpeg/huffman.c ../../chstone/jpeg/jpeg2bmp.c ../../chstone/jpeg/jfif_read.c ../../chstone/jpeg/marker.c &> log2.log
cd ../mips
../../addShBuf ../../chstone/mips/mips.c &> log2.log
cd ../motion
../../addShBuf ../../chstone/motion/mpeg2.c ../../chstone/motion/motion.c ../../chstone/motion/getbits.c ../../chstone/motion/getvlc.c &> log2.log
cd ../sha
../../addShBuf ../../chstone/sha/sha_driver.c ../../chstone/sha/sha.c &> log2.log

cd ..

rename 's/rose_//g' ./*/*.c
